import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'claim',
  templateUrl: 'claim.html'
})
export class ClaimPage implements OnInit {

  claim : FormGroup;

  constructor(public formBuilder: FormBuilder) {

  }

  //To initialise formBuilder, you should implemnt this angularjs2 lifecycle rathter than ionViewLoaded()
  ngOnInit() {
   this.claim = this.formBuilder.group({
     itemName: ['', Validators.required],
     description: ['', Validators.required],
     amount: [0, Validators.required]
   });
 }

 logForm(){
   console.log(this.claim.value)
   console.log(this.claim);
 }
}
