import { Component, ViewChild } from '@angular/core';

import { Platform, MenuController, Nav, Events } from 'ionic-angular';

import { StatusBar } from 'ionic-native';

import { ClaimPage } from '../pages/claim/claim';
import { HistoryPage } from '../pages/history/history';


@Component({
  selector : 'app',
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make HelloIonicPage the root (or first) page
  rootPage: any = ClaimPage;
  pages: Array<{title: string, iconName : string, component: any}>;

  constructor(public platform: Platform, public menu: MenuController, private events : Events) {
    this.initializeApp();

    // set our app's pages
    this.pages = [
      { title: 'Submit a claim', iconName: "create", component: ClaimPage },
      { title: 'History', iconName: "book", component: HistoryPage }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
    });

    this.addListener();
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }

  public addListener() {
    this.events.subscribe("pause", () => {
      console.log("app enter background");
    });

    this.events.subscribe("resume", () => {
      console.log("app resume to foreground");
    });
  }
}
