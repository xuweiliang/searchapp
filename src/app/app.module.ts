import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { ClaimPage } from '../pages/claim/claim';
import { HistoryPage } from '../pages/history/history';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    MyApp,
    ClaimPage,
    HistoryPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    FormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ClaimPage,
    HistoryPage
  ],
  providers: []
})
export class AppModule {}
